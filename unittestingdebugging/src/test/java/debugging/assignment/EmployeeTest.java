package debugging.assignment;

import java.lang.Object;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for Employee Test
 */
public class EmployeeTest 
{
    /**
     * This method checks if equals detects the same when the objects are the same
     * Note: This method should pass after you successfully override the equals method in the Employee class.
     */
    @Test
    public void testEqualsSame()
    {
        Employee e = new Employee("Dan", 12345);
        Employee other = new Employee("Dan", 12345);
        assertEquals(e, other);
    }

        /**
     * This method checks if equals returns false when the names differ.
     * Note: This method should pass after you successfully override the equals method in the Employee class.
     */
    @Test
    public void testEqualsDifferentName()
    {
        Employee e = new Employee("Dan", 12345);
        Employee other = new Employee("NotDan", 12345);
        assertNotEquals(e, other);
    }

            /**
     * This method checks if equals returns false when the ids differ.
     * Note: This method should pass after you successfully override the equals method in the Employee class.
     */
    @Test
    public void testEqualsDifferentId()
    {
        Employee e = new Employee("Dan", 12);
        Employee other = new Employee("Dan", 12345);
        assertNotEquals(e, other);
    }
}
